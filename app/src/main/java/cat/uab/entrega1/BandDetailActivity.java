package cat.uab.entrega1;

import android.app.Activity;
import android.os.Bundle;

import cat.uab.entrega1.common.Constants;
import cat.uab.entrega1.common.MusicBand;
import cat.uab.entrega1.common.MusicBandOperations;

/**
 * Music band detail activity
 */
public class BandDetailActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.band_detail);

    // Band info
    String bandName = getIntent().getStringExtra(Constants.BAND_NAME_KEY);
    MusicBand musicBand = new MusicBand(bandName, this);

    // Draw
    MusicBandOperations operations = new MusicBandOperations(musicBand);
    operations.drawMusicBand();
  }
}
