package cat.uab.entrega1.common;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import java.util.Locale;

/**
 * Represents a music band
 */
public class MusicBand {

  private final String name;
  private final String description;
  private final Drawable picture;

  private final Activity activity;

  /**
   * Constructor
   *
   * @param name     name of the music band
   * @param activity necessary to obtain band music info
   */
  public MusicBand(String name, Activity activity) {
    this.name = name;
    this.activity = activity;

    // Description
    int descriptionID = getResourceId("string", name);
    this.description = activity.getResources().getString(descriptionID);

    // Picture
    int pictureID = getResourceId("drawable", name);
    this.picture = activity.getResources().getDrawable(pictureID);
  }

  /**
   * @return music band name
   */
  public final String getName() {
    return name;
  }

  /**
   * @return music band description
   */
  public final String getDescription() {
    return description;
  }

  /**
   * @return music band picture
   */
  public final Drawable getPicture() {
    return picture;
  }

  /**
   * @return the activity
   */
  public final Activity getActivity() {
    return activity;
  }

  private int getResourceId(String folder, String name) {
    String filteredName = name.toLowerCase(Locale.ENGLISH).replace(' ', '_');
    return activity.getResources().getIdentifier(filteredName, folder, activity.getPackageName());
  }
}
