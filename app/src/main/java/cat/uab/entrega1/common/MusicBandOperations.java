package cat.uab.entrega1.common;

import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import cat.uab.entrega1.R;

/**
 * Operations with a music band
 */
public class MusicBandOperations {

  private final MusicBand musicBand;

  /**
   * Constructor
   *
   * @param musicBand a music band
   */
  public MusicBandOperations(MusicBand musicBand) {
    this.musicBand = musicBand;
  }

  /**
   * Draw music band in fragment
   */
  public final void drawMusicBand() {
    // Activity
    Activity parentActivity = musicBand.getActivity();

    // Set band name
    TextView bandNameText = (TextView) parentActivity.findViewById(R.id.bandName);
    bandNameText.setText(musicBand.getName());

    // Set band description
    TextView bandDescriptionText = (TextView) parentActivity.findViewById(R.id.bandDescription);
    bandDescriptionText.setText(musicBand.getDescription());
    bandDescriptionText.setMovementMethod(new ScrollingMovementMethod());

    // Set band image
    ImageView bandImageView = (ImageView) parentActivity.findViewById(R.id.bandImage);
    bandImageView.setImageDrawable(musicBand.getPicture());
  }
}
