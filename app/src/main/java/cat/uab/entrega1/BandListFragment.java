package cat.uab.entrega1;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Fragment that represents the list of music bands
 */
public class BandListFragment extends Fragment {

  private OnBandSelectedListener bandSelectedListener;

  /**
   * To communicate with the main activity
   */
  public interface OnBandSelectedListener {

    /**
     * Action to do when a band is selected
     *
     * @param bandName name of the music band
     */
    void onBandSelected(String bandName);
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    try {
      // Make sure the activity implements the listener
      bandSelectedListener = (OnBandSelectedListener) activity;

    } catch (ClassCastException exception) {
      Log.e("E", exception.getMessage(), exception);
      throw new IllegalStateException(activity.toString() + " must implement " +
          OnBandSelectedListener.class.getSimpleName());
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.bands_list, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    // Reference to parent activity
    Activity parentActivity = getActivity();

    // Reference to list view
    ListView musicBandsList = (ListView) parentActivity.findViewById(R.id.musicBandsView);

    // Create list
    ArrayAdapter<CharSequence> listAdapter = ArrayAdapter.createFromResource(parentActivity
        .getApplicationContext(), R.array.musicBands, R.layout.band_row);

    // Associate list
    musicBandsList.setAdapter(listAdapter);

    musicBandsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Get band name
        TextView bandTextView = (TextView) view.findViewById(R.id.bandRowName);
        String bandName = bandTextView.getText().toString();

        // Handle action
        bandSelectedListener.onBandSelected(bandName);
      }
    });
  }
}
