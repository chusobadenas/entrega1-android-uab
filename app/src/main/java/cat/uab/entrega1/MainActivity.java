package cat.uab.entrega1;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

import cat.uab.entrega1.common.Constants;

/**
 * Main activity of the application
 */
public class MainActivity extends Activity implements BandListFragment.OnBandSelectedListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  @Override
  public void onBandSelected(String bandName) {
    // PORTRAIT
    if (findViewById(R.id.fragment_container) == null) {
      // Create detail activity
      Intent intent = new Intent(this, BandDetailActivity.class);
      intent.putExtra(Constants.BAND_NAME_KEY, bandName);

      // Launch detail activity
      startActivity(intent);
    }
    // LANDSCAPE
    else {
      // Create fragment
      BandDetailFragment bandDetailFragment = new BandDetailFragment();

      // Pass information to fragment
      Bundle bundle = new Bundle();
      bundle.putString(Constants.BAND_NAME_KEY, bandName);
      bandDetailFragment.setArguments(bundle);

      // Change fragment
      FragmentTransaction transaction = getFragmentManager().beginTransaction();
      transaction.replace(R.id.fragment_container, bandDetailFragment);
      transaction.commit();
    }
  }
}
