package cat.uab.entrega1;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.uab.entrega1.common.Constants;
import cat.uab.entrega1.common.MusicBand;
import cat.uab.entrega1.common.MusicBandOperations;

/**
 * Music band detail fragment
 */
public class BandDetailFragment extends Fragment {

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.band_detail, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    // Parent activity
    Activity parentActivity = getActivity();

    // LANDSCAPE
    if (parentActivity.findViewById(R.id.fragment_container) != null) {
      // Band info
      String bandName = getArguments().getString(Constants.BAND_NAME_KEY);
      MusicBand musicBand = new MusicBand(bandName, parentActivity);

      // Draw
      MusicBandOperations operations = new MusicBandOperations(musicBand);
      operations.drawMusicBand();
    }
  }
}
